# import the necessary packages
from keras.preprocessing import image as image_utils
from keras.applications.imagenet_utils import decode_predictions
from keras.applications.imagenet_utils import preprocess_input
from keras.applications import VGG16
from datetime import datetime
import numpy as np
import argparse
import cv2
import os


def load_image(im_name):
    orig = cv2.imread(im_name)

    image = image_utils.load_img(im_name, target_size=(224, 224))
    image = image_utils.img_to_array(image)

    return orig, image


def preprocess_image(image):
    image = np.expand_dims(image, axis=0)
    image = preprocess_input(image)
    return image


def get_files(dir_name):
    list_of_file = os.listdir(dir_name)
    all_files = list()
    for entry in list_of_file:
        full_path = os.path.join(dir_name, entry)
        if os.path.isdir(full_path):
            all_files = all_files + get_files(full_path)
        else:
            all_files.append(full_path)

    return all_files


def predict(model, image):
    predictions = model.predict(image)
    decoded_predictions = decode_predictions(predictions)
    return decoded_predictions


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", required=True,
                    help="Path to the input image/directory")
    args = ap.parse_args()

    # load model
    model = VGG16(weights="imagenet")
    now = datetime.now()

    try:
        file_handler = open(
            os.path.join(os.path.dirname(args.input), "detected_objects_%s.txt" % (now.strftime("%d_%m_%Y_%H_%M_%S"))),
            'x')
        if os.path.isdir(args.input):
            images = get_files(args.input)
            for img in images:
                orig, image = load_image(img)
                image = preprocess_image(image)
                predictions = predict(model, image)
                _, label, prob = predictions[0][0]
                file_handler.write("Image: {} - Label: {}, {:.2f}%\n".format(img, label, prob * 100))
                # for (i, (imagenetID, label, prob)) in enumerate(predictions[0]):
                #     file_handler.write("Image: {} - Label: {}, {:.2f}%".format(img, label, prob * 100))
        else:
            orig, image = load_image(args.input)
            image = preprocess_image(image)
            predictions = predict(model, image)
            _, label, prob = predictions[0][0]
            file_handler.write("Image: {} - Label: {}, {:.2f}%\n".format(args.input, label, prob * 100))
            # for (i, (imagenetID, label, prob)) in enumerate(predictions[0]):
            #     file_handler.write("Image: {} - Label: {}, {:.2f}%".format(args.input, label, prob * 100))

        file_handler.close()
    except Exception as e:
        print("Oops! Something went wrong.. :( :\n{}".format(e))
